# Mock View

This library provides the `ViewDouble` mock implementation of `View` for use in automated tests.

To use, add as a dev requirement for Composer:

```json
{
  "require-dev": {
    "phpunit/phpunit": "^7",
    "americanreading/view-mock": "^1"
  }
}
```

When using this mock, you can pre-set the output the View will render by passing a string to the constructor or setting the public `output` property.

To assert the view as rendered, inspect the instance's `context` property.

```php
$view = new ViewDouble('PRE-SET OUTPUT');

// Do stuff that should call render()...

$this->assertEquals(
  'Expected value passed in the context', 
  $view->context['item']);
```
