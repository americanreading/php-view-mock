<?php

namespace AmericanReading\View;

class ViewDouble implements View
{
    public $output;
    public $context;

    public function __construct(string $output = '')
    {
        $this->output = $output;
    }

    public function render(array $context): String
    {
        $this->context = $context;
        return $this->output;
    }
}
